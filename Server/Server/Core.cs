﻿using Flag;
using Plugin_Interface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    public class Core
    {
        public static ManualResetEvent allDone = new ManualResetEvent(false);

        public static Plugin_List plugin_List;

        public static List<Socket> listners = new List<Socket>();
        public static List<IPAddress> iPEndPoints = new List<IPAddress>();

        static string log_directory = "Logs";
        static string full_path="";
        static FileStream fileStream;

        public static Dictionary<Socket, string> users = new Dictionary<Socket, string>();

        public static Dictionary<string, string> Login_Nickname = new Dictionary<string, string>();

        public static Dictionary<string, int> Login_Id = new Dictionary<string, int>();

        public static SqlConnection sqlConnection
        {
            get
            {
                return DBWorker.sqlConnection;
            }
        }

        public static bool running = false;

        static int col;

        public static void AddToLog(string msg)
        {
            fileStream = File.Open(full_path, FileMode.OpenOrCreate);
            StreamWriter sw = new StreamWriter(fileStream);
            fileStream.Seek(0, SeekOrigin.End);
            msg = "[" + DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToLocalTime().ToShortTimeString() + "] " + msg;
            sw.WriteLine(msg);
            sw.Close();
        }

        public static void Start(string IP, int port)
        {
            if (!Directory.Exists(log_directory)) Directory.CreateDirectory(log_directory);
            full_path = AppDomain.CurrentDomain.BaseDirectory + log_directory + "\\Log-" + DateTime.Today.ToShortDateString() + "-" + DateTime.Now.ToLocalTime().Hour + "-" + DateTime.Now.ToLocalTime().Minute + "-" + DateTime.Now.ToLocalTime().Second + ".txt";
            bool ready = false;
            while (!ready)
            {
                bool isAvailable = true;
                IPGlobalProperties ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
                TcpConnectionInformation[] tcpConnInfoArray = ipGlobalProperties.GetActiveTcpConnections();
                foreach (TcpConnectionInformation tcpi in tcpConnInfoArray)
                {
                    if (tcpi.LocalEndPoint.Port == port)
                    {
                        isAvailable = false;
                        AddToLog("Порт уже используется");
                        break;
                    }
                }
                plugin_List = new Plugin_List();
                Exception res = plugin_List.Compose();
                if (res != null)
                {
                    AddToLog("Неудалось загрузить плагины");
                    isAvailable = false;
                }
                ready = isAvailable;
            }
            Exception ex = DBWorker.Connect();
            if (ex != null)
            {
                AddToLog("Ошибка подключения к бд");
                Console.WriteLine("Ошибка подключения к бд:");
                Console.WriteLine(ex.ToString());
            }
            else
            {
                RunServer(port, IP);
            }
        }

        public async static void RunServer(int _port, string _ip)
        {
            AddToLog("Сервер запущен");
            running = true;
            await Task.Run(() => StartListening(_port, _ip));
        }

        public static void StartListening(int _port, string _ip)
        {
            IPAddress ipAddress = IPAddress.Parse(_ip);
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, _port);
            Socket listener = new Socket(ipAddress.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(100);
                Console.WriteLine("Waiting for a connection...");
                while (running)
                {
                    allDone.Reset();
                    listener.BeginAccept(
                        new AsyncCallback(AcceptCallback),
                        listener);
                    allDone.WaitOne();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            //TODO: disconnect?
            // Console.WriteLine("\nPress ENTER to continue...");
            //Console.Read();
        }

        public static void AcceptCallback(IAsyncResult ar)
        {
            allDone.Set();
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);
            IPEndPoint handler_ip = handler.RemoteEndPoint as IPEndPoint;
            //TODO: test for same ip 
            // if (!iPEndPoints.Contains(handler_ip.Address))
            {
                iPEndPoints.Add(handler_ip.Address);
                listners.Add(handler);
                StateObject state = new StateObject();
                state.workSocket = handler;
                if (state.workSocket.Connected)
                {
                    state.name = col.ToString();
                    col++;
                    AddToLog("Подключен новый пользователь "+handler_ip);
                    handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                       new AsyncCallback(ReadCallback), state);
                }
            }
            /* else
             {
                 Console.WriteLine("Данный сокет подключен");
                 handler.Shutdown(SocketShutdown.Both);
                 handler.Close();
             }*/
        }

        public static void ReadCallback(IAsyncResult ar)
        {
            String content = String.Empty;
            StateObject state = (StateObject)ar.AsyncState;
            Socket handler = state.workSocket;
            int bytesRead = 0;
            
            try
            {
                bytesRead = handler.EndReceive(ar);
                if (bytesRead > 0)
                {
                    state.sb.Append(Encoding.UTF8.GetString(
                        state.buffer, 0, bytesRead));
                    content = state.sb.ToString();
                    if (content.IndexOf("<EOM>") > -1)
                    {
                        AddToLog("Получено от " + (handler.RemoteEndPoint as IPEndPoint)+"/сообщение: "+ content);
                        content = ServerConverter.ConvertToRead(content);
                        string name = ServerConverter.GetAllParams(content)[0];
                        //Console.WriteLine(state.name);
                        //first coonection
                        if (!users.ContainsKey(handler))
                        {
                            users.Add(handler, name);
                        }
                             /*foreach (var item in users)
                             {
                                 if (item.Value!=content)
                                 Send(item.Key, Converter.ConvertToSend("Подключен новый пользователь "+content));
                             }
                             state.first_msg = false;
                             //Console.WriteLine("Подключен новый пользователь");
                         }*/
                        //Console.WriteLine("Read {0} bytes from socket. \n Data : {1}",
                        //    content.Length, content);
                        //Chat messages
                        /*foreach (var item in users)          
                        {
                            if (item.Key != handler)
                                Send(item.Key, Converter.ConvertToSend(users[handler]+": " + content));

                        }*/
                        // ReadCommand(content, handler);
                        ReadCommand(content, state.workSocket);
                        state.sb.Clear();

                    }
                    handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReadCallback), state);
                    //Send(handler, content);

                }
                else
                {
                    Console.WriteLine("Подключение закрыто");
                    AddToLog((handler.RemoteEndPoint as IPEndPoint) + " отключен");
                    DeleteIp(handler);
                }
            }
            catch (Exception ex)
            {
                // ShowConnectionsCount();
                AddToLog((handler.RemoteEndPoint as IPEndPoint) + " экстренно отключен");
                Console.WriteLine("Подключение экстренно закрыто");
                Console.WriteLine(ex.ToString());
                DeleteIp(handler);
            }

        }

        public static void DeleteIp(Socket _socket)
        {
            IPEndPoint socket_ip = _socket.RemoteEndPoint as IPEndPoint;
            iPEndPoints.Remove(socket_ip.Address);
            foreach (var item in users)
            {
                if (item.Key == _socket)
                {
                    users.Remove(item.Key);
                    return;
                }
            }
            listners.Remove(_socket);
        }

        private static void CloseSocket(Socket _socket)
        {

            IPEndPoint socket_ip = _socket.RemoteEndPoint as IPEndPoint;
            iPEndPoints.Remove(socket_ip.Address);

            _socket.Shutdown(SocketShutdown.Both);
            _socket.Close();
        }

        public static void Disconnect(string Login)
        {
            Login_Id.Remove(Login);
            Login_Nickname.Remove(Login);
            //users.
        }

        private static void ReadCommand(string _content, Socket handler)
        {
            plugin_List.Execute(_content, users[handler], handler);
        }

        public static void SendWOConvert(Socket handler, String data)
        {
            byte[] byteData = Encoding.UTF8.GetBytes(data);
            Console.WriteLine(data);
            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), handler);
        }

        public static void Send(Socket handler, String data)
        {
            //todo: проверить нужно ли конвертить при отправке
           // data = ServerConverter.ConvertToSend(data);
            byte[] byteData = Encoding.UTF8.GetBytes(data);
            Console.WriteLine(data);
            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), handler);
        }

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                Socket handler = (Socket)ar.AsyncState;
                int bytesSent = handler.EndSend(ar);
                Console.WriteLine("Sent {0} bytes to client.", bytesSent);

                //handler.Shutdown(SocketShutdown.Both);
                // handler.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static void UpdatePlayers(object x)
        {
            //players = Connector.SelectAllPlayers(true);
            //foreach (string item in Connector.SelectAllPlayers(true))
            {
                //    players_str += item + ",";
            }
            foreach (Socket item in listners)
            {
                //  Send(item,Converter.ConvertToSend(players_str,Flags.Send));
            }
        }

        internal static void ShowPlugins()
        {
            foreach (var plugin in plugin_List._Plugins)
            {
                Console.WriteLine(plugin.Command);
            }
        }

        internal static void StopServer()
        {
            //Exception ex = Connector.Close_Connect();
            //if (ex == null)
            {
                Console.WriteLine("БД отключена");
            }
            // else
            {
                //     Console.WriteLine("Ошибка отключения БД: " + ex.ToString());
            }
            foreach (Socket item in listners)
            {
                CloseSocket(item);
            }
            listners.Clear();
            running = false;
        }

        public static void ShowPlayers(bool _onlineOnly)
        {
            Console.WriteLine("Зарегистрированные пользователи:");
            //  foreach (string nick in Connector.SelectAllPlayers(_onlineOnly))
            {
                // Console.WriteLine(nick);
            }
        }

        public static void ShowConnectionsCount()
        {

        }

        public static void ShowConnections()
        {
            Console.WriteLine("Подключенные пользователи:");
            foreach (IPAddress item in iPEndPoints)
            {
                Console.WriteLine(item);
            }
        }
    }
}