﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Server
{
    class DBWorker
    {
        internal static SqlConnection sqlConnection;
        public static string connectionString = @"Integrated Security=SSPI;Persist Security Info=False;User ID=;Initial Catalog= TF Data;Data Source=;Initial File Name=";

        public static Exception TestConnection()
        {
            sqlConnection = new SqlConnection(connectionString);
            try
            {
                sqlConnection.Open();
                sqlConnection.Close();
            }
            catch (Exception ex)
            {
                return ex;
            }
            return null;
        }

        public static Exception Connect()
        {
            sqlConnection = new SqlConnection(connectionString);
            try
            {
                sqlConnection.Open();
            }
            catch (Exception ex)
            {
                return ex;
            }
            return null;
        }

        public static Exception Close_Connect()
        {
            try
            {
                sqlConnection.Close();
            }
            catch (Exception ex)
            {
                return ex;
            }
            return null;
        }

        public static List<string> SelectAllPlayers(bool _onlineOnly = false)
        {
            List<string> results = new List<string>();
            SqlDataReader sqlReader = null;
            string cmd = "";
            if (_onlineOnly)
            {
                cmd = "SELECT * FROM [Players] WHERE OnlineStatus=1";
            }
            else
            {
                cmd = "SELECT * FROM [Players]";
            }
            SqlCommand command = new SqlCommand(cmd, sqlConnection);
            try
            {
                sqlReader = command.ExecuteReader();
                while (sqlReader.Read())
                {
                    results.Add(Convert.ToString(sqlReader["Nickname"]));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (sqlReader != null)
                    sqlReader.Close();

            }
            return results;
        }
    }
}
