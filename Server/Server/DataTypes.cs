﻿using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace Server
{
    //TODO: Check for delete
    class ListWithNoShifting<T>
    {
        private List<T> list;
        private List<int> freeIndexes;

        public ListWithNoShifting()
        {
            list = new List<T>();
            freeIndexes = new List<int>();
        }

        public T this[int _pos] {
            get {
                return list[_pos];
            }
        }

        public void Remove(int index)
        {
            list[index] = default(T);
            freeIndexes.Add(index);
            freeIndexes.Sort();
        }

        public int Add(T item)
        {
            int result = -1;
            if (freeIndexes.Count == 0)
            {
                list.Add(item);
                result = list.Count - 1;
            }
            else
            {
                list[freeIndexes[0]] = item;
                result = freeIndexes[0];
                freeIndexes.RemoveAt(0);
            }
            return result;
        }
    }

    public class StateObject
    {
        public Socket workSocket = null;
        public const int BufferSize = 1024;
        public byte[] buffer = new byte[BufferSize];
        public StringBuilder sb = new StringBuilder();
        public bool first_msg = true;

        //for test
        public string name = string.Empty;
    }
}
