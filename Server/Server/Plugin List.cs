﻿using Plugin_Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using Flag;
using System.Net.Sockets;

namespace Server
{
    public class Plugin_List
    {
        private CompositionContainer _container;
        [ImportMany(typeof(IPlugin))]
        public List<IPlugin> _Plugins = new List<IPlugin>();
        public Dictionary<Flags, IPlugin> plugins = new Dictionary<Flags, IPlugin>();

        public Exception Compose()
        {
            Exception ex = null;
            if (_container != null)
            {
                _container.Dispose();
                _Plugins.Clear();
                plugins.Clear();
            }
            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new DirectoryCatalog("..\\Debug"));
            //Console.WriteLine(new DirectoryCatalog("..\\Debug").FullPath);
            _container = new CompositionContainer(catalog);

            try
            {
                this._container.ComposeParts(this);
            }
            catch (CompositionException compositionException)
            {
                ex=compositionException;
            }
            foreach (var item in _Plugins)
            {
                //TODO: Adding to List
                plugins.Add(item.Command, item);
            }
            return ex;
        }

        public void Execute(string _command, string name, Socket socket)
        {
            ReqData data = new ReqData(_command, name, socket);
            plugins[ServerConverter.GetFlag(_command)].Execute(data);
        }
    }
}
