﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Shell
    {
        public static int Main(String[] args)
        {
            string ip = string.Empty;
            int port = -1;

            // Console.Write("Введите IP адресс: ");
            ip = "127.0.0.1";

            //Console.Write("Введите порт: ");
            //int.TryParse(Console.ReadLine(), out port);
            port = 10;

            Core.Start(ip, port);
            while (Core.running) 
            {
                string command = Console.ReadLine();
                switch (command.Split(' ')[0])
                {
                    case "stop":
                        {
                            Core.StopServer();
                            break;
                        }
                    case "show":
                        {
                            Core.ShowConnections();
                            Core.ShowPlayers(true);
                            break;
                        }
                    case "col":
                        {
                            Core.ShowConnectionsCount();
                            break;
                        }
                    case "plugins":
                        {
                            Core.ShowPlugins();
                            break;
                        }
                }
            }
            return 0;
        }
    }
}
