﻿using Flag;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class Client : MonoBehaviour
{
    private string _IP;
    private int _port;

    [SerializeField]
    public GameObject UI;

    [SerializeField]
    public GameObject FightUI;

    private FightUI _fightUIScript;

    public static Client instance;

    private bool _inGame = false;
    public bool clientStarted;
    private Socket _clientSocket;

    private static bool _listening = false;

    public static string startLocation;

    private static ManualResetEvent _connectDone;
    private static ManualResetEvent _sendDone;

    public class StateObject
    {
        public Socket workSocket = null;
        public const int BufferSize = 256;
        public byte[] buffer = new byte[BufferSize];
        public StringBuilder sb = new StringBuilder();
        public string result = String.Empty;
    }

    private void Awake()
    {
        if (instance == null)
            instance = this;
        _fightUIScript = FightUI.GetComponent<FightUI>();
        Account.OnAuthorization += BecomeOnline;
    }

    public void StartClient()
    {
        try
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(_IP);
            //Dns.GetHostEntry(Dns.GetHostName());

            /* foreach (var item in ipHostInfo.AddressList)
             {
                 if (!item.IsIPv6LinkLocal && !item.IsIPv4MappedToIPv6 && !item.IsIPv6Multicast && !item.IsIPv6SiteLocal && !item.IsIPv6Teredo)
                 {
                     ipAddress = item;
                     break;
                 }
             }*/
            IPAddress ipAddress = ipHostInfo.AddressList[1];
            // IPAddress ipAddress = IPAddress.Parse(_IP);
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, _port);
            _clientSocket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            _connectDone = new ManualResetEvent(false);
            _clientSocket.BeginConnect(remoteEP, new AsyncCallback(ConnectCallback), _clientSocket);
            _connectDone.WaitOne();
            clientStarted = true;
        }
        catch (Exception e)
        {
            Debug.LogError(e.ToString());
        }
    }

    private void ConnectCallback(IAsyncResult ar)
    {
        try
        {
            Socket clientSoc = (Socket)ar.AsyncState;
            clientSoc.EndConnect(ar);
            IPEndPoint client_ip = clientSoc.RemoteEndPoint as IPEndPoint;
            _connectDone.Set();
        }
        catch (Exception e)
        {
            Debug.LogError(e.ToString());
        }
    }

    public void StartListening()
    {
        _listening = true;
        StateObject state = new StateObject();
        try
        {
            state.workSocket = _clientSocket;
            _clientSocket.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(ReceiveAnswerCallback), state);
        }
        catch (Exception e)
        {
            Debug.LogError(e.ToString());
        }
    }

    private void ReceiveAnswerCallback(IAsyncResult ar)
    {
        string content = "";
        try
        {
            StateObject state = (StateObject)ar.AsyncState;
            Socket clientSoc = state.workSocket;
            int bytesRead = clientSoc.EndReceive(ar);
            if (bytesRead > 0)
            {
                state.sb.Append(Encoding.UTF8.GetString(state.buffer, 0, bytesRead));
                content = state.sb.ToString();
                //Debug.Log(content);
                if (content.IndexOf("<EOM>") > -1)
                {
                    // response = state.sb.ToString();
                    state.result = ClientConverter.ConvertToRead(state.sb.ToString());
                    //Debug.Log(Converter.ConvertToRead(state.sb.ToString()));
                    //return;
                    ServerAnswersStorage.AddMessage(state.result);
                    //TasksManager.AddJob(() => MessageController.Add(state.result));
                    state.sb.Clear();
                    //receiveDone.Set();
                }
            }
            //if (listening)
            clientSoc.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
               new AsyncCallback(ReceiveAnswerCallback), state);
        }
        catch (Exception e)
        {
            Debug.LogError(e.ToString());
        }
    }

    public async void Send(string data)
    {
        if (!clientStarted)
            await Task.Run(() => StartClient());
        byte[] byteData = Encoding.UTF8.GetBytes(data);
        _sendDone = new ManualResetEvent(false);
        if (!_listening)
            await Task.Run(() => StartListening());
        _clientSocket.BeginSend(byteData, 0, byteData.Length, 0,
            new AsyncCallback(SendCallback), _clientSocket);
        _sendDone.WaitOne();
    }

    private void SendCallback(IAsyncResult ar)
    {
        try
        {
            Socket clientSoc = (Socket)ar.AsyncState;
            int bytesSent = clientSoc.EndSend(ar);
            _sendDone.Set();
        }
        catch (Exception e)
        {
            Debug.LogError(e.ToString());
        }
    }

    public void StopClient()
    {
        if (clientStarted)
        {
            _clientSocket.Shutdown(SocketShutdown.Both);
            _listening = false;
            clientStarted = false;
        }
    }

    private void BecomeOnline(ServerAnswersStorage.ServerAnswer serverAnswer)
    {
        SetOnlineStatus(true);
    }

    public void SetOnlineStatus(bool value)
    {
        _inGame = value;
    }

    public void SetEndPoint(string IP, int port)
    {
        _IP = IP;
        _port = port;
    }

    public bool GetOnlineStatus()
    {
        return _inGame;
    }

    private void Disconnect()
    {
        Send(ClientConverter.ConvertToSend(Flags.Disconnect));
    }

    private void OnApplicationQuit()
    {
        // Disconnect(Nickname);
        if (_inGame)
            Disconnect();
        StopClient();
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void Attack()
    {
        Send(ClientConverter.ConvertToSend(Flags.Attack));
        ServerAnswersStorage.OnMessageGet += ShowAttack;
    }

    private void ShowAttack(Flags flag)
    {
        if (flag == Flags.Attack)
        {
            ServerAnswersStorage.ServerAnswer answer = ServerAnswersStorage.GetAnswer(flag);
            if (answer.Flag == Flags.OK)
            {
                TasksManager.AddJob(() => _fightUIScript.Attack());

            }
            ServerAnswersStorage.OnMessageGet -= ShowAttack;
        }
    }

    public void StopFight()
    {
        TasksManager.AddJob(() => UI.gameObject.SetActive(true));
        TasksManager.AddJob(() => FightUI.gameObject.SetActive(false));
    }

    public void UpdateStats(string message)
    {
        Send(ClientConverter.ConvertToSend(Flags.UpdateStats, message));
        ServerAnswersStorage.OnMessageGet += UpdatePlayerStats;
    }

    private void UpdatePlayerStats(Flags flag)
    {
        if (flag == Flags.UpdateStats)
        {
            ServerAnswersStorage.ServerAnswer answer = ServerAnswersStorage.GetAnswer(flag);
            if (answer.Flag == Flags.OK)
            {
                TasksManager.AddJob(() => CharacterButton.uIButton.ChangesToPlayerStats());
            }
        }
        ServerAnswersStorage.OnMessageGet -= UpdatePlayerStats;
    }

   /* private void RefreshStats(Flags flag)
    {
        if (flag == Flags.RefreshStats)
        {
            MessageController.Answer answer = MessageController.GetAnswer(flag);
            List<string> strings = MessageController.SplitMessage(answer.Message);
            foreach (var item in strings)
            {
                string[] splits = item.Split('-');
                Player.Stat stat = new Player.Stat(splits[1], Convert.ToInt32(splits[2]));
                Player.player.playerStats.Add(Convert.ToInt32(splits[0]), stat);
            }

            foreach (var item in Player.player.playerStats.Keys)
            {
                Debug.Log(item + " " + Player.player.playerStats[item].Name + " " + Player.player.playerStats[item].Count);

            }
            MessageController.OnMessageGet -= RefreshStats;
        }
    }*/

    public void SendTest(string text)
    {
        Send(ClientConverter.ConvertToSend(Flags.Recieve, text));
    }
}
