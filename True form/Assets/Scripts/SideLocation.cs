﻿using UnityEngine;
using UnityEngine.EventSystems;

public class SideLocation : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public GameObject outline;
    public string locationName;
    public int locationId;
    [Range(0, 1)]
    public int type;
    public enum LocationTypes { Location, Fight };

    public void OnPointerClick(PointerEventData eventData)
    {
        Move();
    }

    public void Move()
    {
        ScenesAdministrator.instance.ChangeLocation(this);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        outline.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        outline.SetActive(false);
    }
}
