﻿using UnityEngine;
using UnityEngine.EventSystems;

public class TransferHighlighter : MonoBehaviour, IPointerEnterHandler,IPointerExitHandler
{
    public GameObject img;
    
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (img != null) img.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (img != null) img.SetActive(false);
    }
}
