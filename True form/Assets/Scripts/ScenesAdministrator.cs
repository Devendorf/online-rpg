﻿using Flag;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesAdministrator : MonoBehaviour
{
    public static ScenesAdministrator instance;

    private SideLocation _currentLocation;

    public delegate void FirstLoad();
    public static event FirstLoad OnFirstLoad;

    private void Start()
    {
        if (instance == null)
            instance = this;
        Account.OnAuthorization += LoadStartLocation;
    }

    private void LoadStartLocation(ServerAnswersStorage.ServerAnswer serverAnswer)
    {
        string locationName = serverAnswer.Message;
        LoadLevel(locationName, true);
    }

    public void LoadLevel(string locationName, bool startLocation = false)
    {
        SceneManager.LoadScene(locationName);
        if (startLocation)
            SceneManager.sceneLoaded += OnFirstSceneLoad;
        else
            SceneManager.sceneLoaded += OnSceneLoad;
    }

    private void OnFirstSceneLoad(Scene arg0, LoadSceneMode arg1)
    {
        BaseLoad(arg0);
        OnFirstLoad?.Invoke();
        SceneManager.sceneLoaded -= OnFirstSceneLoad;
    }

    private void OnSceneLoad(Scene arg0, LoadSceneMode arg1)
    {
        BaseLoad(arg0);
        SceneManager.sceneLoaded -= OnSceneLoad;
    }

    private void BaseLoad(Scene scene)
    {
        LocationListLoader.instance.currentLocationName.text = scene.name;
        LocationListLoader.instance.LoadList();
    }

    public void ChangeLocation(SideLocation target)
    {
        _currentLocation = target;
        if (target.type == Convert.ToInt32(SideLocation.LocationTypes.Location))
        {
            Client.instance.Send(ClientConverter.ConvertToSend(Flags.Go, target.locationId.ToString()));
            ServerAnswersStorage.OnMessageGet += TryLoadLocation;
        }
        else
        {
            StartFight();
        }
    }

    private void TryLoadLocation(Flags flagReq)
    {
        if (flagReq == Flags.Go)
        {
            ServerAnswersStorage.ServerAnswer answer = ServerAnswersStorage.GetAnswer(flagReq);
            if (answer.Flag == Flags.OK)
            {
                LoadLevel(_currentLocation.locationName);
            }
            if (answer.Flag == Flags.Err)
            {
                Messages.ShowMessage(answer.Message);
            }
            ServerAnswersStorage.OnMessageGet -= TryLoadLocation;
        }
    }

    public void StartFight()
    {
        Client.instance.Send(ClientConverter.ConvertToSend(Flags.StartFight, _currentLocation.locationId.ToString()));
        //Debug.Log(ClientConverter.ConvertToSend(Flags.StartFight, locationId.ToString()));
        ServerAnswersStorage.OnMessageGet += LoadFight;
    }

    private void LoadFight(Flags flag)
    {
        if (flag == Flags.StartFight)
        {
            ServerAnswersStorage.ServerAnswer answer = ServerAnswersStorage.GetAnswer(flag);
            if (answer.Flag == Flags.OK)
            {
                //TasksManager.AddJob(() => UI.gameObject.SetActive(false));
                //TasksManager.AddJob(() => FightUI.gameObject.SetActive(true));
                List<Unit> units = new List<Unit>();
                List<string> strings = ServerAnswersStorage.SplitMessage(answer.Message);
                foreach (var item in strings)
                {
                    string[] splits = item.Split('-');
                    Unit unit = new Unit
                    {
                        // Name = splits[0],
                        // Health = Convert.ToInt32(splits[1])
                    };
                    units.Add(unit);
                }
                //TasksManager.AddJob(() => fightUIScript.StartFight(units));
            }
            ServerAnswersStorage.OnMessageGet -= LoadFight;
        }
    }
}
