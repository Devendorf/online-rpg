﻿using UnityEngine;
using UnityEngine.UI;

public class PlayersListAtLoc : MonoBehaviour
{
    [SerializeField] private Transform _playersList;
    [SerializeField] private GameObject _playerTextPref;

    private void Start()
    {
        Updater.OnUpdate += Refresh;
    }

    public void Refresh(string[] playersNames)
    {
        foreach (var player in _playersList.GetComponentsInChildren<Button>())
        {
            Destroy(player.gameObject);
        }
        foreach (var playerName in playersNames)
        {
            GameObject obj = Instantiate(_playerTextPref, _playersList);
            Button btn = obj.GetComponent<Button>();
            btn.GetComponentInChildren<Text>().text = playerName;
        }
    }
}
