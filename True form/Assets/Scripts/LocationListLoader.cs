﻿using UnityEngine;
using UnityEngine.UI;

public class LocationListLoader : MonoBehaviour
{
    [SerializeField] public Text currentLocationName;
    [SerializeField] private GameObject _movementLocationsPanel;
    [SerializeField] private GameObject _listItem;

    public static LocationListLoader instance;

    private void Start()
    {
        if (instance == null)
            instance = this;
    }

    public void LoadList()
    {
        ClearList();
        foreach (var location in FindObjectsOfType<SideLocation>())
        {
            GameObject obj = Instantiate(_listItem, _movementLocationsPanel.transform);
            Button btn = obj.GetComponent<Button>();
            btn.GetComponentInChildren<Text>().text = location.locationName;
            btn.onClick.AddListener(location.Move);
            TransferHighlighter SFLobj = obj.GetComponent<TransferHighlighter>();
            SFLobj.img = location.outline;
        }
    }

    private void ClearList()
    {
        foreach (var listItem in _movementLocationsPanel.GetComponentsInChildren<Button>())
        {
            Destroy(listItem.gameObject);
        }
    }
}
