﻿using System.Collections.Generic;
using UnityEngine;

public class FightUI : MonoBehaviour
{
    [SerializeField]
    private GameObject _attackUList;

    [SerializeField]
    private GameObject _defenceUList;

    [SerializeField]
    private GameObject _unitPref;

    private List<Unit> _units = new List<Unit>();

    public void StartFight(List<Unit> units)
    {
        ClearLists();
        _units = units;
        GameObject attackUnitObj = Instantiate(_unitPref, _attackUList.transform);
        Unit attackUnit = attackUnitObj.GetComponent<Unit>();
        attackUnit.Copy(_units[0]);
        _units.RemoveAt(0);
        foreach (var unit in _units)
        {
            GameObject defenceUnitObj = Instantiate(_unitPref, _defenceUList.transform);
            Unit defenceUnit = defenceUnitObj.GetComponent<Unit>();
            defenceUnit.Copy(unit);
        }
    }

    private void ClearLists()
    {
        foreach (var item in _attackUList.GetComponentsInChildren<Unit>())
        {
            Destroy(item.gameObject);
        }
        foreach (var item in _defenceUList.GetComponentsInChildren<Unit>())
        {
            Destroy(item.gameObject);
        }
    }

    public void Attack()
    {
        Unit unit = _defenceUList.GetComponentInChildren<Unit>();
        Destroy(unit.gameObject);
        if (_defenceUList.transform.childCount == 1)
        {
            Client.instance.StopFight();
        }
    }
}
