﻿using Flag;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public struct Stat
    {
       public string Name;
       public int Count;

        public void ChangeCount(int count)
        {
            Count = count;
        }

        public Stat(string name, int count)
        {
            Name = name;
            Count = count;
        }
    }

    public Dictionary<int, Stat> playerStats = new Dictionary<int, Stat>();
   
    public static Player instance;

    void Start()
    {
        Account.OnAuthorization += CreatePlayer;
    }

    private void CreatePlayer(ServerAnswersStorage.ServerAnswer serverAnswer)
    {
        if (instance == null)
        {
            instance = this;
            Client.instance.Send(ClientConverter.ConvertToSend(Flags.RefreshStats));
            ServerAnswersStorage.OnMessageGet += RefreshStats;
        }
    }

   /* private static void RefreshStats(Flags flag)
    {
        if (flag == Flags.RefreshStats)
        {
            MessageController.ServerAnswer answer = MessageController.GetAnswer(flag);
            List<string> strings = MessageController.SplitMessage(answer.Message);
            foreach (var item in strings)
            {
                string[] splits = item.Split('-');
                Stat stat = new Stat(splits[1], Convert.ToInt32(splits[2]));
                instance.playerStats.Add(Convert.ToInt32(splits[0]), stat);
            }

            foreach (var item in instance.playerStats.Keys)
            {
                Debug.Log(item + " " + instance.playerStats[item].Name + " " + instance.playerStats[item].Count);

            }
            MessageController.OnMessageGet -= RefreshStats;
        }
    }  */
    private void RefreshStats(Flags flag)
    {
        if (flag == Flags.RefreshStats)
        {
            ServerAnswersStorage.ServerAnswer answer = ServerAnswersStorage.GetAnswer(flag);
            List<string> strings = ServerAnswersStorage.SplitMessage(answer.Message);
            foreach (var item in strings)
            {
                string[] splits = item.Split('-');
                Stat stat = new Stat(splits[1], Convert.ToInt32(splits[2]));
                playerStats.Add(Convert.ToInt32(splits[0]), stat);
            }
            /*foreach (var item in playerStats.Keys)
            {
                Debug.Log(item + " " + playerStats[item].Name + " " + playerStats[item].Count);
            }*/
            ServerAnswersStorage.OnMessageGet -= RefreshStats;
        }
    }
}
