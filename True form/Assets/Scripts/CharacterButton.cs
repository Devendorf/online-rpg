﻿using System.Collections.Generic;
using UnityEngine;

public class CharacterButton : SwitchableButton
{
    public static CharacterButton uIButton;

    [SerializeField] private GameObject _content;
    [SerializeField] private GameObject _prefab;

    public bool needUpdate = false;

    protected override void Start()
    {
        base.Start();
        uIButton = this;
    }

    public void ChangesToPlayerStats()
    {
        foreach (var stat in _content.GetComponentsInChildren<StatControl>())
        {
            Player.instance.playerStats[stat.StatId] = new Player.Stat(Player.instance.playerStats[stat.StatId].Name, stat.StatNumber);
            //Player.player.playerStats[VARIABLE.StatId].ChangeCount(VARIABLE.StatNumber);
        }
    }

    public void ConfirmChanges()
    {
        List<string> strings = new List<string>();
        foreach (var stat in _content.GetComponentsInChildren<StatControl>())
        {
            if (stat.BaseStatNumber != stat.StatNumber)
            {
                strings.Add(stat.StatId.ToString() + '-' + stat.StatNumber.ToString());
            }
        }
        if (strings.Count > 0)
        {
            string message = string.Empty;
            foreach (var item in strings)
            {
                message += item + '/';
            }
            if (message != "")
               message = message.Remove(message.Length - 1);
                Client.instance.UpdateStats(message);
        }
    }

        public void LoadList()
    {
        foreach (var stat in _content.GetComponentsInChildren<StatControl>())
        {
            Destroy(stat.gameObject);
        }
        foreach (var statKey in Player.instance.playerStats.Keys)
        {
            GameObject obj = Instantiate(_prefab, _content.transform);
            StatControl statControl = obj.GetComponent<StatControl>();
            statControl.StatName = Player.instance.playerStats[statKey].Name;
            statControl.StatNumber=  Player.instance.playerStats[statKey].Count;
            statControl.BaseStatNumber = Player.instance.playerStats[statKey].Count;
            statControl.StatId = statKey;
            statControl.UpdateText();
           // btn.onClick.AddListener(VARIABLE.ChangeLocation);
            //ShowFromList SFLobj = obj.GetComponent<ShowFromList>();
            //SFLobj.img = VARIABLE.outline;
        }
    }

    public override void Click()
    {
        base.Click();
        if (IsWindowActive())
            LoadList();
    }
}
