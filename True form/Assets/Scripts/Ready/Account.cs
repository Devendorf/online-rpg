﻿using Flag;
using System;
using System.Security.Cryptography;
using System.Text;

public class Account
{

    public delegate void SuccessesAuthorization(ServerAnswersStorage.ServerAnswer serverAnswer);
    public static event SuccessesAuthorization OnAuthorization;

    public static void TryToLogin(string login, string pass)
    {
        Client.instance.Send(GetLoginCommand(login, pass));
        ServerAnswersStorage.OnMessageGet += EnterToGame;
    }
    
    public static void TryToRegister(string login, string pass, string nick)
    {
        Client.instance.Send(GetRegisterCommand(login, pass, nick));
        ServerAnswersStorage.OnMessageGet += EnterToGame;
    }

    private static string GetLoginCommand(string login, string pass)
    {
        string command = "";
        command += login + ',' + CalculateHash(pass);
        return ClientConverter.ConvertToSend(Flags.LogIn, command);
    }

    private static string GetRegisterCommand(string login, string pass, string nick)
    {
        string command = "";
        command += login + ',' + CalculateHash(pass) + ',' + nick;
        return ClientConverter.ConvertToSend(Flags.Registration, command);
    }

    private static string CalculateHash(string text)
    {
        byte[] data = Encoding.Default.GetBytes(text);
        var result = new SHA256Managed().ComputeHash(data);
        return BitConverter.ToString(result).Replace("-", "").ToLower();
    }

    private static void EnterToGame(Flags flag)
    {
        if (flag == Flags.LogIn || flag == Flags.Registration)
        {
            ServerAnswersStorage.ServerAnswer answer = ServerAnswersStorage.GetAnswer(flag);
            if (answer.Flag == Flags.OK)
            {
                //Messages.ShowMessage(answer.Message);
                if (OnAuthorization != null)
                    OnAuthorization.Invoke(answer);
            }
            //Messages.ShowMessage(answer.Message);
            if (answer.Flag == Flags.Err)
            {
                Messages.ShowMessage(answer.Message);
                Client.instance.StopClient();
            }
            ServerAnswersStorage.OnMessageGet -= EnterToGame;
        }
    }
}
