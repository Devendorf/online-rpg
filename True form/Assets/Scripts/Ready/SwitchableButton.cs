﻿using UnityEngine;
using UnityEngine.UI;

public class SwitchableButton : MonoBehaviour
{
    [SerializeField] private GameObject _window;
    private Button _thisBtn;

    protected virtual void Start()
    {
        _thisBtn = gameObject.GetComponent<Button>();
    }

    public bool IsWindowActive()
    {
        return _window.activeSelf;
    }

    public virtual void Click()
    {
        _window.SetActive(!_window.activeSelf);
        Color color;
        if (!_window.activeSelf)
            color = _thisBtn.colors.normalColor;
        else
        {
            color = _thisBtn.colors.pressedColor;
        }
        _thisBtn.image.color = color;
    }
}
