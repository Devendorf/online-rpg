﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(InputField))]
public class Ip_Mask : MonoBehaviour
{
    private InputField _inputField;
    private void Start()
    {
        _inputField = GetComponent<InputField>();
    }

    public void Check(string text)
    {
        string result = string.Empty;
        foreach (var item in text)
        {
            if (item == '.' || char.IsDigit(item))
            {
                result += item;
            }
        }
        _inputField.text = result;
    }
}
