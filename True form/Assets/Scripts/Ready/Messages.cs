﻿using UnityEngine;
using UnityEngine.UI;

public class Messages : MonoBehaviour
{
    [SerializeField]
    private GameObject _msgBox;

    [SerializeField]
    private Text _msgBoxText;

    private static Messages _instance;

    private void Start()
    {
        _instance = this;
    }

    public static void ShowMessage(string Message)
    {
        _instance._msgBox.SetActive(true);
        _instance._msgBoxText.text = Message;
    }
}
