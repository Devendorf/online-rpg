﻿public interface IReadyCheck
{
   bool ready { get; }
}
