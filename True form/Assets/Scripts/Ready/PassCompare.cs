﻿using UnityEngine;
using UnityEngine.UI;

public class PassCompare : MonoBehaviour, IReadyCheck
{
    [SerializeField] private GameObject _errMsg;
    [SerializeField] private InputField _pass1;
    [SerializeField] private InputField _pass2;
    [SerializeField] private NullDisabler _nullDisabler;

    public bool ready { get; private set; } = false;

    private void Start()
    {
        _nullDisabler.otherConditions.Add(this);
    }

    public void Check(string text)
    {
        ready = _pass2.text == _pass1.text;
        _errMsg.SetActive(!ready);
    }
}
