﻿using System;
using System.Collections;
using Flag;
using UnityEngine;

public class Updater : MonoBehaviour
{
    [SerializeField] private float _refreshTime;
    private bool _started;

    public static Updater instance;

    public delegate void Update(string[] info);
    public static event Update OnUpdate;

    private void Start()
    {
        if (instance == null)
            instance = this;
        _started = false;
        ScenesAdministrator.OnFirstLoad += StartUpdating;
    }

    private void StartUpdating()
    {
        if (!_started)
        {
            StartCoroutine(SendUpdateReq());
            _started = true;
           // StopUpdating();
        }
        ScenesAdministrator.OnFirstLoad -= StartUpdating;
    }

    private void StopUpdating()
    {
        StopCoroutine(SendUpdateReq());
        try { ServerAnswersStorage.OnMessageGet -= UpdateData; }
        catch (Exception ex) { Debug.Log(ex.ToString()); }
    }

    IEnumerator SendUpdateReq()
    {
        while (true)
        {
            Client.instance.Send(ClientConverter.ConvertToSend(Flags.Update));
            ServerAnswersStorage.OnMessageGet += UpdateData;
            yield return new WaitForSeconds(_refreshTime);
        }
    }

    private void UpdateData(Flags flag)
    {
        if (flag == Flags.Update)
        {
            ServerAnswersStorage.ServerAnswer answer = ServerAnswersStorage.GetAnswer(flag);
            string[] players = answer.Message.Split('/');
            //Debug.Log(answer.Message);
            //TasksManager.instance.AddJob(()=>_playersListAtLoc.Refresh(players));
            OnUpdate?.Invoke(players);
            ServerAnswersStorage.OnMessageGet -= UpdateData;
        }
    }
}
