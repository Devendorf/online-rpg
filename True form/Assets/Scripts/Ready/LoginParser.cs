﻿using UnityEngine;
using UnityEngine.UI;

public class LoginParser : Parser
{
    [SerializeField] private InputField _login, _password;

    protected override void FillList()
    {
        base.FillList();
        _fields.Add(_login);
        _fields.Add(_password);
    }

    protected override void ExecuteParseEvent()
    {
        Account.TryToLogin(_login.text, _password.text);
    }
}
