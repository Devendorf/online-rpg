﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class NullDisabler : MonoBehaviour
{
    [SerializeField]
    private InputField[] _notNullFields;
    private Button _button;

    public List<IReadyCheck> otherConditions = new List<IReadyCheck>();

    private void Start()
    {
        _button = GetComponent<Button>();
    }

    void Update()
    {
        bool ready = true;
        foreach (InputField item in _notNullFields)
        {
            ready = ready && item.text != "";
        }
        foreach (IReadyCheck item in otherConditions)
        {
            ready = ready && item.ready;
        }
        _button.interactable = ready;
    }
}
