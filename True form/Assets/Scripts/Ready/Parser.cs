﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public abstract class Parser : MonoBehaviour
{
    protected List<InputField> _fields = new List<InputField>();

    [SerializeField] private InputField _ip, _port;

    private Button _parseButton;

    private void Start()
    {
        _parseButton = GetComponent<Button>();
        FillList();
    }

    protected virtual void FillList()
    {
        _fields.Add(_ip);
        _fields.Add(_port);
    }

    protected abstract void ExecuteParseEvent();

    public void Parse()
    {
        bool fill = false;
        foreach (var field in _fields)
        {
            if (field.text == string.Empty)
            {
                Debug.LogError("Не все поля заполнены");
                break;
            }
            fill = true;
        }
        if (fill)
        {
            _parseButton.interactable = false;
            Client.instance.SetEndPoint(_ip.text, int.Parse(_port.text));
            ExecuteParseEvent();
        }
    }
}
