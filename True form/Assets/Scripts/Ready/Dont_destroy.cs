﻿using UnityEngine;

public class Dont_destroy : MonoBehaviour
{

    [SerializeField]
    private bool _needOff;

    void Start()
    {
        DontDestroyOnLoad(gameObject);
        if (_needOff) gameObject.SetActive(false);
    }
}
