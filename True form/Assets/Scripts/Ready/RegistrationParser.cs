﻿using UnityEngine;
using UnityEngine.UI;

public class RegistrationParser : Parser
{
    [SerializeField]
    private InputField _login, _password1, _password2, _nickname;

    protected override void FillList()
    {
        base.FillList();
        _fields.Add(_login);
        _fields.Add(_password1);
        _fields.Add(_password2);
        _fields.Add(_nickname);
    }

    protected override void ExecuteParseEvent()
    {
        Account.TryToRegister(_login.text, _password1.text, _nickname.text);
    }
}
