﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TasksManager: MonoBehaviour
{
    private static Queue<Action> _tasks = new Queue<Action>();

    private void Update()
    {
        while (_tasks.Count > 0)
        {
            _tasks.Dequeue().Invoke();
        }
    }
    public static void AddJob(Action newTask)
    {
        _tasks.Enqueue(newTask);
    }

}
