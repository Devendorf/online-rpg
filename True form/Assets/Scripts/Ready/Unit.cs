﻿using UnityEngine;
using UnityEngine.UI;

public class Unit : MonoBehaviour
{
    [SerializeField]
    private Text _nameText;
    [SerializeField]
    private Text _healthText;

    public string Name
    {
        get { return Name; }
        set
        {
            Name = value;
            _nameText.text = Name;
        }
    }
    public string Health
    {
        get { return Health; }
        set
        {
            Health = value;
            _healthText.text = Health;
        }
    }

    public void SetUp(string name, string health)
    {
        Name = name;
        Health = health;
    }

    public void Copy(Unit unit)
    {
        SetUp(unit.Name, unit.Health);
    }
}
