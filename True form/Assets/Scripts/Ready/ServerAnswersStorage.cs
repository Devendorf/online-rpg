﻿using Flag;
using System;
using System.Collections.Generic;
using UnityEngine;

public static class ServerAnswersStorage
{
    public struct ServerAnswer
    {
        public Flags Flag;
        public string Message;

        public ServerAnswer(Flags flag, string message)
        {
            Flag = flag;
            Message = message;
        }
    }

    private static Dictionary<Flags, ServerAnswer> _serverMessages = new Dictionary<Flags, ServerAnswer>();

    public delegate void MessageEventHandler(Flags command);
    public static event MessageEventHandler OnMessageGet;

    public static List<string> SplitMessage(string message)
    {
        List<string> result = new List<string>();
        foreach (var item in message.Split('/'))
        {
            result.Add(item);
        }
        return result;
    }

    public static void AddMessage(string answer)
    {
        string[] parts = ClientConverter.GetAllParts(answer);
        int index = 0;
        Enum.TryParse(parts[index], out Flags commandFlag);
        index++;
        if (Enum.TryParse(parts[index], out Flags resultFlag))
            index++;
        string message=String.Empty;
        if (parts.Length>index)
             message= parts[index];
        //Debug.Log(commandFlag+" "+resultFlag+" "+message);
        _serverMessages.Add(commandFlag, new ServerAnswer(resultFlag, message));
        TasksManager.AddJob(() => OnMessageGet(commandFlag));
    }

    public static bool hasAnswer(Flags command)
    {
        return _serverMessages.ContainsKey(command);
    }

    public static ServerAnswer GetAnswer(Flags command)
    {
        if (hasAnswer(command))
        {
            ServerAnswer answer = _serverMessages[command];
            _serverMessages.Remove(command);
            return answer;
        }
        else
        {
            Debug.LogError($"Не найдено сообщение сервера с командой {command}");
            return new ServerAnswer();
        }
    }
}
