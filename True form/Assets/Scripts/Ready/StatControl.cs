﻿using UnityEngine;
using UnityEngine.UI;

public class StatControl : MonoBehaviour
{

    [SerializeField] private Button _addBtn;
    [SerializeField] private Button _removeBtn;
    [SerializeField] private Text _text;

    public int StatId;
    public int BaseStatNumber;
    public int StatNumber;
    public string StatName;

    public void UpdateText()
    {
        _text.text = StatName + ": " + StatNumber;
    }

    public void Add()
    {
        StatNumber++;
        UpdateText();
    }

    public void Remove()
    {
        if (StatNumber > 0)
        {
            StatNumber--;
            UpdateText();
        }
    }
}
