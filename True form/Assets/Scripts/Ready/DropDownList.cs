﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class DropDownList : MonoBehaviour
{
    [SerializeField]
    private GameObject _list;

    private Button _switchButton;
    private GameObject _arrowUp, _arrowDown;

    [SerializeField]
    private bool _open = true;

    void Start()
    {
        _switchButton = GetComponent<Button>();
        _switchButton.onClick.AddListener(Switch);
        _arrowUp = transform.GetChild(0).gameObject;
        _arrowDown = transform.GetChild(1).gameObject;
    }

    void Switch()
    {
        _open = !_open;
        _list.SetActive(_open);
        _arrowUp.SetActive(_open);
        _arrowDown.SetActive(!_open);
    }
}
