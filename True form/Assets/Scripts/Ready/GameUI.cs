﻿using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{

    [SerializeField] private List<GameObject> _invisibleObjects;
    void Start()
    {
        Account.OnAuthorization += ActivateUI;
    }

    private void ActivateUI(ServerAnswersStorage.ServerAnswer serverAnswer)
    {
        foreach (var gameObject in _invisibleObjects)
        {
            gameObject.SetActive(true);
        }
    }
}
