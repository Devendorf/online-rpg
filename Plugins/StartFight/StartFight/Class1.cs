﻿using Flag;
using Plugin_Interface;
using Server;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace StartFight
{
    public class StartFight : IPlugin
    {
        public Flags Command => Flags.StartFight;

        struct Mob
        {
            public string Name;
            public int Health;
        }

        public void Execute(ReqData data)
        {
            string location_id=string.Empty;
            string message = ServerConverter.GetAllMessage(data.Request);
            SqlCommand command = new SqlCommand("SELECT  Id_Location from [Characters] where Id_Character=" + Core.Login_Id[data.Name], Core.sqlConnection);
            try
            {
                location_id = command.ExecuteScalar().ToString();
               
            }
            catch { }
            SqlCommand command2 = new SqlCommand("Monster_Spawn_From_Location", Core.sqlConnection);
            command2.CommandType = System.Data.CommandType.StoredProcedure;
            command2.Parameters.AddWithValue("@Id_Loc", Convert.ToInt32(location_id));
            command2.Parameters.AddWithValue("@Number", Convert.ToInt32(message));
            string spawnID = string.Empty;
            int min=-1, max=-1;
            SqlDataReader sqlReader = null;
            try
            {
                sqlReader = command2.ExecuteReader();
                sqlReader.Read();
                spawnID =Convert.ToString(sqlReader["Id_Monster_spawn"]);
                min= Convert.ToInt32(sqlReader["Min_count"]);
                max = Convert.ToInt32(sqlReader["Max_count"]);
            }
            catch(Exception ex) { Console.WriteLine(ex.ToString());}
            sqlReader.Close();
            Random rnd = new Random();
            int mobCount = rnd.Next(min, max+1);
            string mobId = "";
            List<Mob> mobs = new List<Mob>();
            SqlCommand sqlCommand3 = new SqlCommand("SELECT Monster_groups.Id_Monster FROM Monster_groups WHERE Monster_groups.Id_Monster_spawn=" + spawnID, Core.sqlConnection);
            mobId = sqlCommand3.ExecuteScalar().ToString();
            //Console.WriteLine(mobCount);
            SqlCommand sqlCommand4 = new SqlCommand("Mob_Unit", Core.sqlConnection);
            sqlCommand4.CommandType = System.Data.CommandType.StoredProcedure;
            sqlCommand4.Parameters.AddWithValue("@mobId", Convert.ToInt32(mobId));
            List<string> units = new List<string>();
            SqlDataReader sqlReader3 = null;
            for (int i = 0; i < mobCount; i++)
            {
                sqlReader3= sqlCommand4.ExecuteReader();
                sqlReader3.Read();
                units.Add(sqlReader3["Id_Unit"].ToString());
                sqlReader3.Close();
            }
           
            int unitsCount =mobCount;
            List<string> pairs = new List<string>();
            SqlCommand sqlCommand5 = new SqlCommand("Create_Pairs", Core.sqlConnection);
            sqlCommand5.CommandType = System.Data.CommandType.StoredProcedure;
            sqlCommand5.Parameters.AddWithValue("@Count",Convert.ToInt32(unitsCount));
            SqlDataReader sqlReader4 = null;
            try
            {
                sqlReader4 = sqlCommand5.ExecuteReader();
                while (sqlReader4.Read())
                {
                    pairs.Add(Convert.ToString(sqlReader4["Id_Pair"]));
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            sqlReader4.Close();
            SqlCommand sqlCommand6 = new SqlCommand("Create_Fight", Core.sqlConnection);
            sqlCommand6.CommandType = System.Data.CommandType.StoredProcedure;
            SqlDataReader sqlReader2= null;
            List<string> groups = new List<string>();
            try
            {
                sqlReader2 = sqlCommand6.ExecuteReader();
                while (sqlReader2.Read())
                {
                    groups.Add(Convert.ToString(sqlReader2["Id_Group"]));
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            sqlReader2.Close();
            
            foreach (var item in groups)
            {
                Console.WriteLine(item);
            }
            foreach (var item in units)
            {
                SqlCommand sqlCommand7 = new SqlCommand("UPDATE Units SET Id_Group =" + groups[0] + " WHERE Id_Unit=" +item, Core.sqlConnection);
                sqlCommand7.ExecuteNonQuery();
            }
            for (int i=0;i<units.Count;i++)
            {
                SqlCommand sqlCommand11 = new SqlCommand("UPDATE Units SET Id_Pair =" + pairs[i] + " WHERE Id_Unit=" + units[i], Core.sqlConnection);
                sqlCommand11.ExecuteNonQuery();
            }
            SqlCommand sqlCommand9 = new SqlCommand("SELECT Id_Unit FROM Units WHERE Id_base =" + Core.Login_Id[data.Name] + " AND Is_monster = 0", Core.sqlConnection);
            int PlayerUnitId =Convert.ToInt32(sqlCommand9.ExecuteScalar());
            SqlCommand sqlCommand8 = new SqlCommand("UPDATE Units SET Id_Group =" + groups[1] + " WHERE Id_Unit="+PlayerUnitId, Core.sqlConnection);
            sqlCommand8.ExecuteNonQuery();
            string answer = string.Empty;
            SqlCommand sqlCommand10 = new SqlCommand("SELECT (dbo.Heath_Count (" + PlayerUnitId+"))", Core.sqlConnection);
            //sqlCommand10.CommandType = System.Data.CommandType.StoredProcedure;
            //sqlCommand10.Parameters.AddWithValue("@UnitID", PlayerUnitId);
            int playerHP = Convert.ToInt32(sqlCommand10.ExecuteScalar());
            answer = data.Name + '-' + playerHP+'/';
            
            foreach (var item in units)
            {
                SqlCommand sqlCommand11 = new SqlCommand("SELECT (dbo.Mob_Name (" + item + "))", Core.sqlConnection);
                string Name = Convert.ToString(sqlCommand11.ExecuteScalar());
                SqlCommand sqlCommand12 = new SqlCommand("SELECT (dbo.Heath_Count (" + item + "))", Core.sqlConnection);
                int HP = Convert.ToInt32(sqlCommand12.ExecuteScalar());
                answer += Name + '-' + HP + '/';
            }
            if (answer != "")
                answer = answer.Remove(answer.Length - 1);
            Core.Send(data.Socket, ServerConverter.ConvertToSend(Flags.StartFight, Flags.OK,answer));
        }
    }
}
