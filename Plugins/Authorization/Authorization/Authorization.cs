﻿using System;
using Plugin_Interface;
using System.Data.SqlClient;
using Server;
using Flag;

namespace Authorization
{
    public class Authorization : IPlugin
    {
        public Flags Command => Flags.LogIn;

        public void Execute(ReqData data)
        {
            //int result = -1;
            string[] parametres = ServerConverter.GetAllParams(data.Request);
            string login = parametres[0];
            string pass = parametres[1];
            bool existPlayer = false;
            string reqPass = String.Empty;
            SqlCommand command = new SqlCommand("Get_Password_From_Login", Core.sqlConnection);
            command.Parameters.AddWithValue("@Login", login);
            try
            {
                reqPass = command.ExecuteScalar().ToString();
                existPlayer = String.IsNullOrEmpty(reqPass);
                if (pass == reqPass)
                {
                    string char_id = String.Empty;
                    string nick = String.Empty;
                    string location_id = String.Empty;
                    SqlCommand command2 = new SqlCommand("Get_Character_Id_From_Login", Core.sqlConnection);
                    command2.Parameters.AddWithValue("@Login", login);
                    try
                    {
                        char_id = command2.ExecuteScalar().ToString();
                        Core.Login_Id.Add(login, Convert.ToInt32(char_id));

                    }
                    catch { }
                    SqlCommand command3 = new SqlCommand("SELECT Name, Id_Location from [Characters] where Id_Character=" + char_id, Core.sqlConnection);
                    try
                    {
                        sqlReader2 = command3.ExecuteReader();
                        while (sqlReader2.Read())
                        {
                            nick = Convert.ToString(sqlReader2["Name"]);
                            location_id = Convert.ToString(sqlReader2["Id_Location"]);
                            Core.Login_Nickname.Add(login, nick);
                        }
                        sqlReader2.Close();
                    }
                    catch { }
                    finally
                    {
                        if (sqlReader != null)
                            sqlReader.Close();
                        if (sqlReader2 != null)
                        {
                            sqlReader2.Close();
                        }
                    }
                    SqlCommand command4 = new SqlCommand("UPDATE [Characters] SET Is_online=1 WHERE Id_Character= " + char_id, Core.sqlConnection);
                    try
                    {
                        command4.ExecuteNonQuery();
                        // result = 1;
                    }
                    catch (Exception ex)
                    {
                        string asd = ex.ToString();
                        Console.WriteLine(asd);
                    }
                    string location = String.Empty;
                    Console.WriteLine("Подключен новый пользователь");
                    SqlCommand command5 = new SqlCommand("SELECT Name FROM [Locations]  WHERE Id_Location= " + location_id, Core.sqlConnection);
                    try
                    {
                        sqlReader2 = command5.ExecuteReader();
                        while (sqlReader2.Read())
                        {
                            Console.WriteLine(location);
                            location = Convert.ToString(sqlReader2["Name"]);
                        }
                        sqlReader2.Close();
                    }
                    catch { }
                    //SqlCommand command6 = new SqlCommand("Список_Характеристик",Core.sqlConnection);
                    //command6.CommandType = System.Data.CommandType.StoredProcedure;     
                    //SqlDataReader sqlReader3 = null;
                    //command6.Parameters.AddWithValue("@Id_Character", char_id);
                    //try
                    //{
                    //    sqlReader3 = command6.ExecuteReader();
                    //    while (sqlReader3.Read())
                    //    {
                    //        Console.WriteLine("asd"+Convert.ToString(sqlReader3["Value"]));
                    //    }
                    //    sqlReader3.Close();
                    //}
                    //catch { }
                    Core.Send(data.Socket, ServerConverter.ConvertToSend(Command, Flags.OK, location));
                    // result = 1;
                    //TODO: Результат авторизации
                    //SqlCommand command2 = new SqlCommand("UPDATE [Players] SET OnlineStatus=1 WHERE Nickname='" + _nickname + "'", sqlConnection);
                    //command2.ExecuteNonQuery();
                }
                else
                {
                    // result = 0;
                    Console.WriteLine("Неверный пароль");
                    Core.Send(data.Socket, ServerConverter.ConvertToSend(Command, Flags.Err, "Неверный пароль"));
                    //Core.Disconnect(new RespData(data,""));
                    //TODO: Код вывода ошибки
                };

            }
            catch (Exception ex)
            {
                string asd = ex.ToString();
            }
            finally
            {
                if (!existPlayer)
                {
                    Core.Send(data.Socket, ServerConverter.ConvertToSend(Command, Flags.Err, "Неверный логин"));
                    Console.WriteLine("Неверный логин");
                }
                if (sqlReader != null)
                    sqlReader.Close();
            }
        }
    }
}
