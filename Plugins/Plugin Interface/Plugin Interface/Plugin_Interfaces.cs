﻿using System.ComponentModel.Composition;
using System.Data.SqlClient;
using System.Net.Sockets;
using Flag;

namespace Plugin_Interface
{
    [InheritedExport]
    public interface IPlugin
    {
        Flags Command { get;}
        void Execute(ReqData data);
    }

    public class RespData
    {
        public string Msg;
        public string Name;

        public RespData(string message, string _name)
        {
            Msg= message;
            Name = _name;
        }

        public RespData(ReqData reqData, string message)
        {
            Name = reqData.Name;
            Msg = message;
        }
    }

    public class ReqData
    {
        public string Request;
        public string Name;
        public Socket Socket;

        public ReqData(string requestLine, string _name, Socket _socket)
        {
            Request = requestLine;
            Name = _name;
            Socket = _socket;
        }
    }
}
