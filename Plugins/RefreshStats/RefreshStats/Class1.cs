﻿using System;
using System.Collections.Generic;
using Plugin_Interface;
using System.Data.SqlClient;
using Server;
using Flag;

namespace RefreshStats
{
    public class RefreshStats : IPlugin
    {
        public Flags Command => Flags.RefreshStats;

        public void Execute(ReqData data)
        {
            Dictionary<int, string> stats = new Dictionary<int, string>();
            SqlCommand command = new SqlCommand("Get_Stats_List", Core.sqlConnection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            SqlDataReader sqlReader = null;
            command.Parameters.AddWithValue("@Id_Character", Core.Login_Id[data.Name]);
            try
            {
                sqlReader = command.ExecuteReader();
                while (sqlReader.Read())
                {
                    stats.Add(Convert.ToInt32(sqlReader["Id_Stat_type"]), sqlReader["Name"].ToString() + '-' + sqlReader["Value"].ToString());
                    // Console.WriteLine(Convert.ToString(sqlReader["Value"]));
                }
                sqlReader.Close();
            }
            catch { }
            string message = String.Empty;
            foreach (var VARIABLE in stats.Keys)
            {
                message += VARIABLE.ToString() + '-' + stats[VARIABLE].ToString() + '/';    // "/"- for struct split ("-"- for variable split)
                Console.WriteLine(message);
            }
            if (message != "")
                message = message.Remove(message.Length - 1);
            Core.Send(data.Socket, ServerConverter.ConvertToSend(Command, message));
            //   Console.WriteLine(ServerConverter.ConvertToSend(Command, message));
        }
    }
}
