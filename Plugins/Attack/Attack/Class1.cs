﻿using Flag;
using Plugin_Interface;
using Server;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Attack
{
    public class Attack : IPlugin
    {
        public Flags Command => Flags.Attack;

        public void Execute(ReqData data)
        {
            SqlCommand sqlCommand = new SqlCommand("Kill_Mob", Core.sqlConnection);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            sqlCommand.Parameters.AddWithValue("@PlayerId", Core.Login_Id[data.Name]);
            sqlCommand.ExecuteNonQuery();
            Core.Send(data.Socket, ServerConverter.ConvertToSend(Flags.Attack, Flags.OK));
        }
    }
}
