﻿using Flag;
using Plugin_Interface;
using Server;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Registration
{
    public class Registration : IPlugin
    {
        public Flags Command => Flags.Registration;

        public void Execute(ReqData data)
        {
            string[] allParams = ServerConverter.GetAllParams(data.Request);
            bool existLogin = false;
            bool existName = false;
            string key = allParams[0];
            string str1 = allParams[1];
            string str2 = allParams[2];
            SqlDataReader sqlDataReader1 = (SqlDataReader)null;
            SqlCommand sqlCommand1 = new SqlCommand("SELECT Password FROM [Users] WHERE Login='" + key + "'", Core.sqlConnection);
            try
            {
                sqlDataReader1 = sqlCommand1.ExecuteReader();
                while (sqlDataReader1.Read())
                    existLogin = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                if (existLogin)
                    Core.Send(data.Socket, ServerConverter.ConvertToSend(this.Command, Flag.Flags.Err, "Данный логин уже зарегистрирован"));
                sqlDataReader1?.Close();
            }
            if (!existLogin)
            {
                SqlDataReader sqlDataReader2 = (SqlDataReader)null;
                SqlCommand sqlCommand2 = new SqlCommand("SELECT Name FROM [Characters] WHERE Name='" + str2 + "'", Core.sqlConnection);
                try
                {
                    sqlDataReader2 = sqlCommand2.ExecuteReader();
                    while (sqlDataReader2.Read())
                        existName = true;
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }
                finally
                {
                    if (existName)
                        Core.Send(data.Socket, ServerConverter.ConvertToSend(this.Command, Flag.Flags.Err, "Данный никнейм занят"));
                    sqlDataReader2?.Close();
                }
            }
            if (existName || existLogin)
                return;
            SqlCommand sqlCommand3 = new SqlCommand(nameof(Registration), Core.sqlConnection);
            sqlCommand3.CommandType = CommandType.StoredProcedure;
            try
            {
                sqlCommand3.Parameters.AddWithValue("@Login", (object)key);
                sqlCommand3.Parameters.AddWithValue("@Pass", (object)str1);
                sqlCommand3.Parameters.AddWithValue("@Nick", (object)str2);
                sqlCommand3.ExecuteNonQuery();
                Core.Login_Nickname.Add(key, str2);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            SqlCommand sqlCommand4 = new SqlCommand("SELECT Id_Character FROM [Characters] WHERE Name='" + str2 + "'", Core.sqlConnection);
            try
            {
                int int32 = Convert.ToInt32(sqlCommand4.ExecuteScalar());
                Console.WriteLine(int32);
                Core.Login_Id.Add(key, int32);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            string startLocation = string.Empty;
            SqlCommand command5 = new SqlCommand("Get_Start_Location", Core.sqlConnection);
            try
            {
                startLocation = command5.ExecuteScalar().ToString();
            }
            catch { }
            Core.Send(data.Socket, ServerConverter.ConvertToSend(this.Command, Flag.Flags.OK, startLocation));
        }
    }
}