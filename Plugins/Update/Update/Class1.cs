﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Flag;
using Plugin_Interface;
using Server;

namespace Update
{
    public class Update:IPlugin
    {
        public Flags Command => Flags.Update;
        
        public void Execute(ReqData data)
        {
            List<string> playersAtThisLoc= new List<string>();
            string nickname = Core.Login_Nickname[data.Name];
            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT Name FROM Characters WHERE Id_Location=(SELECT Characters.Id_Location FROM Characters WHERE Name='" + nickname + "') AND Is_online=1", Core.sqlConnection);
            try
            {
                sqlReader = command.ExecuteReader();
                while (sqlReader.Read())
                {
                    string player = Convert.ToString(sqlReader["Name"]);
                    if (nickname != player)
                        playersAtThisLoc.Add(player);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                string message=String.Empty;
                foreach (var VARIABLE in playersAtThisLoc)
                {
                    message += VARIABLE + '/';    // "/"- for struct split ("-"- for variable split)
                }
                if (message!="")
                    message = message.Remove(message.Length - 1);
                // Core.Send(data.Socket,ServerConverter.ConvertToSend(Command,message));
                Core.Send(data.Socket, ServerConverter.ConvertToSend(Command, message));
                // Console.WriteLine(ServerConverter.ConvertToSend(Command,message));
                sqlReader?.Close();
            }
        }
    }
}