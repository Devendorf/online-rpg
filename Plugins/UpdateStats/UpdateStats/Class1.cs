﻿using System;
using System.Collections.Generic;
using Plugin_Interface;
using System.Data.SqlClient;
using Server;
using Flag;

namespace UpdateStats
{
    public class UpdateStats : IPlugin
    {
        public Flags Command => Flags.UpdateStats;

        public void Execute(ReqData data)
        {
            bool done = true;
            //Console.WriteLine(data.Request);
            string message = ServerConverter.GetAllMessage(data.Request);
            List<string> pairs = new List<string>();

            foreach (var item in message.Split('/'))
            {
                pairs.Add(item);
            }

            foreach (var item in pairs)
            {
                string[] values = item.Split('-');
                Console.WriteLine(item);
                SqlCommand command = new SqlCommand("Update_Stat", Core.sqlConnection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Id_Character", Core.Login_Id[data.Name]);
                command.Parameters.AddWithValue("@Id_Stat_type", Convert.ToInt32(values[0]));
                command.Parameters.AddWithValue("@Value", Convert.ToInt32(values[1]));
                try
                {
                    command.ExecuteNonQuery();
                }
                catch { done = false; }
            }
            if (done)
                Core.Send(data.Socket,ServerConverter.ConvertToSend(Command, Flags.OK));
            //   Dictionary<int, string> stats = new Dictionary<int, string>();
            //   SqlCommand command = new SqlCommand("Список_Характеристик", Core.sqlConnection);
            //   command.CommandType = System.Data.CommandType.StoredProcedure;
            //   SqlDataReader sqlReader = null;
            //   command.Parameters.AddWithValue("@Id_Character", Core.Login_Id[data.Name]);
            //   try
            //   {
            //       sqlReader = command.ExecuteReader();
            //       while (sqlReader.Read())
            //       {
            //           stats.Add(Convert.ToInt32(sqlReader["Id_Stat_type"]), sqlReader["Name"].ToString() + '-'+sqlReader["Value"].ToString());
            //          // Console.WriteLine(Convert.ToString(sqlReader["Value"]));
            //       }
            //       sqlReader.Close();
            //   }
            //   catch { }
            //   string message = String.Empty;
            //   foreach (var VARIABLE in stats.Keys)
            //   {
            //       message += VARIABLE.ToString()+'-'+stats[VARIABLE].ToString() + '/';    // "/"- for struct split ("-"- for variable split)
            //       Console.WriteLine(message);
            //   }
            //   if (message != "")
            //       message = message.Remove(message.Length - 1);
            //   Core.Send(data.Socket, ServerConverter.ConvertToSend(Command, message));
            ////   Console.WriteLine(ServerConverter.ConvertToSend(Command, message));
        }
    }
}
