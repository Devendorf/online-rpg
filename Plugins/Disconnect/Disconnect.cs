﻿using Flag;
using Plugin_Interface;
using Server;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disconnect
{
    public class Disconnect : IPlugin
    {
        public Flags Command => Flags.Disconnect;

        public void Execute(ReqData data)
        {
            SqlCommand command = new SqlCommand("UPDATE [Characters] SET Is_online=0 WHERE Name= '" + Core.Login_Nickname[data.Name] + "'", Core.sqlConnection);
            try
            {
                command.ExecuteNonQuery();
                // result = 1;
            }
            catch (Exception ex)
            {
                string asd = ex.ToString();
                Console.WriteLine(asd);
            }
            Core.Disconnect(data.Name);
        }
    }
}
