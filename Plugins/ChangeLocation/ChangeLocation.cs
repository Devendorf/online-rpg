﻿using Flag;
using Plugin_Interface;
using Server;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChangeLocation
{
    public class ChangeLocation : IPlugin
    {
        public Flags Command => Flags.Go;

        public void Execute(ReqData data)
        {
            string[] parametres = ServerConverter.GetAllParams(data.Request);
            string loc_to_id = parametres[0];
            string loc_from_id = "";
            bool can = false;
            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT Id_Location FROM [Characters] WHERE Name='" + Core.Login_Nickname[data.Name] + "'", Core.sqlConnection);
            try
            {
                sqlReader = command.ExecuteReader();
                while (sqlReader.Read())
                {
                    loc_from_id = Convert.ToString(sqlReader["Id_Location"]);
                }
                sqlReader.Close();
            }
            catch (Exception ex)
            {
                string asd = ex.ToString();
            }
            SqlCommand command2 = new SqlCommand("SELECT Id_Location_from,Id_Location_to FROM [Paths] WHERE Id_Location_from=(SELECT Id_Location FROM [Characters] WHERE Name='" + Core.Login_Nickname[data.Name] + "')", Core.sqlConnection);
            try
            {
                sqlReader = command2.ExecuteReader();
                while (sqlReader.Read())
                {
                    if (loc_from_id == Convert.ToString(sqlReader["Id_Location_from"]))
                    {
                        if (loc_to_id == Convert.ToString(sqlReader["Id_Location_to"]))
                        {
                            can = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string asd = ex.ToString();
            }
            finally
            {
                if (sqlReader != null)
                    sqlReader.Close();
            }
            if (can)
            {
                SqlCommand command3 = new SqlCommand("UPDATE [Characters] SET Id_Location=" + loc_to_id+ " WHERE Name= '" + Core.Login_Nickname[data.Name]+"'", Core.sqlConnection);
                try
                {
                    command3.ExecuteNonQuery();
                    // result = 1;
                }
                catch (Exception ex)
                {
                    string asd = ex.ToString();
                    Console.WriteLine(asd);
                }
                Core.Send(data.Socket, ServerConverter.ConvertToSend(Command, Flags.OK));
            }
            else
            {
                Core.Send(data.Socket, ServerConverter.ConvertToSend(Command, Flags.Err, "WTF"));
            }
            if (sqlReader != null)
                sqlReader.Close();
        }
    }
}
