﻿using System;

namespace Flag
{
    public enum Flags
    {
        None,
        Send,
        Recieve,
        Disconnect,
        GiveMeInfoPls,
        LogIn,
        Registration,
        SendAndTake,
        OK,
        Err,
        Go,
        Update,
        UpdateStats,
        RefreshStats,
        StartFight,
        Attack
    }

    public class ClientConverter
    {
        public static string ConvertToRead(string _msg)
        {
            return _msg = _msg.Replace("<EOM>", "");
        }

        public static string[] GetAllParts(string _msg)
        {
            //string flag = _msg.Split(',')[0] + ',';
            _msg = _msg.Replace("<EOM>", "");
            return _msg.Split(',');
        }

        ///<params name="_flag">Тип флага</params>>
        ///<params name="_id">Уникальный идентификатор пользователя</params>>
        ///<params name="_msg">Сообщение для передачи</params>>
        public static string ConvertToSend(Flags _flag,int _id, string _msg)
        {
            return _msg = _flag.ToString() + ',' + _id.ToString() + ',' + _msg + "<EOM>";
        }

        public static string ConvertToSend(Flags _flag, string _msg)
        {
            return _msg = _flag.ToString() + ',' + _msg + "<EOM>";
        }

        ///<params name="_flag">Тип флага</params>>
        ///<params name="_id">Уникальный идентификатор пользователя</params>>
        ///<params name="_params">Набор данных для передачи</params>>
        public static string ConvertToSend(Flags _flag,int _id, string[] _params)
        {
            string parametrs = string.Empty;
            foreach (var item in _params)
            {
                parametrs += item + ',';
            }

            return _flag.ToString() + ',' + _id.ToString() + ',' + parametrs.Remove(parametrs.Length - 1) + "<EOM>";
        }

        public static string ConvertToSend(Flags _flag, string[] _params)
        {
            string parametrs = string.Empty;
            foreach (var item in _params)
            {
                parametrs += item + ',';
            }

            return _flag.ToString() + ','  + parametrs.Remove(parametrs.Length - 1) + "<EOM>";
        }
        
        public static string ConvertToSend(Flags _flag)
        {
            return _flag.ToString() + "," + "<EOM>";
        }

        public static Flags GetFlag(string _msg)
        {
            string command= _msg.Split(',')[0];
            Flags flag;
            if (Enum.TryParse(command, out flag))
                return flag;
            else return Flags.None;
        }

        public static int GetID(string _msg)
        {
            return int.Parse(_msg.Split(',')[1]);
        }

        /// <summary>
        /// Метод для извлечения одного значения из полученного сообщения
        /// </summary>
        /// <param name="_msg">Сообщение</param>
        /// <param name="_id">Номер параметра в сообщении</param>
        /// <returns>Возвращает одно значение из сообщения</returns>
        public static string GetValue(string _msg, int _id)
        {
            return _msg.Split(',')[_id + 1];
        }

        /// <summary>
        ///Метод для получения всего сообщения без флага 
        /// </summary>
        /// <param name="_msg">Полученное сообщение</param>
        /// <returns>Готовое сообщение</returns>
        public static string GetAllMessage(string _msg)
        {
            string flag = _msg.Split(',')[0] + ',';
            return _msg = _msg.Replace("<EOM>", "").Replace(flag, "");
        }
    }

    public class ServerConverter
    {
        public static string ConvertToRead(string _msg)
        {
            return _msg = _msg.Replace("<EOM>", "");
        }

        public static string ConvertToSend(Flags _flag, string _msg)
        {
            return _msg = _flag.ToString() + "," + _msg + "<EOM>";
        }

        public static string ConvertToSend(Flags command, Flags result, string _msg)
        {
            return command.ToString() + "," + result.ToString() + ","+ _msg + "<EOM>";
        }
        public static string ConvertToSend(Flags command, Flags result)
        {
            return  command.ToString() + "," + result.ToString() + "<EOM>";
        }

        public static string ConvertToSend(string _msg)
        {
            return _msg + "<EOM>";
        }

        public static string ConvertToSend(Flags _flag, int _playerID, string _msg)
        {
            return _msg = _flag.ToString() + "," + _playerID.ToString() + ',' + _msg + "<EOM>";
        }

        public static string ConvertToSend(Flags _flag)
        {
            return _flag.ToString() + "," + "<EOM>";
        }

        public static string ConvertToSend(Flags _flag, int _playerID, string[] _params)
        {
            string parametrs = string.Empty;
            foreach (var item in _params)
            {
                parametrs += item + ',';
            }

            return _flag.ToString() + ',' + _playerID + ',' + parametrs.Remove(parametrs.Length - 1) + "<EOM>";
        }

       /* public static string GetFlag(string _msg)
        {
            return _msg.Split(',')[0];
        }
        */
        public static Flags GetFlag(string _msg)
        {
            string command = _msg.Split(',')[0];
            Flags flag;
            if (Enum.TryParse(command, out flag))
                return flag;
            else return Flags.None;
        }

        public static int GetID(string _msg)
        {
            return int.Parse(_msg.Split(',')[1]);
        }

        /// <summary>
        /// Метод для извлечения одного значения из полученного сообщения
        /// </summary>
        /// <param name="_msg">Сообщение</param>
        /// <param name="_id">Номер параметра в сообщении</param>
        /// <returns>Возвращает одно значение из сообщения</returns>
        public static string GetValue(string _msg, int _id)
        {
            return _msg.Split(',')[_id + 1];
        }

        /// <summary>
        ///Метод для получения всего сообщения без флага 
        /// </summary>
        /// <param name="_msg">Полученное сообщение</param>
        /// <returns>Готовое сообщение</returns>
        public static string GetAllMessage(string _msg)
        {
            string flag = _msg.Split(',')[0] + ',';
            return _msg = _msg.Replace("<EOM>", "").Replace(flag, "");
        }
        /// <summary>
        /// Метод для получения всех параметров из сообщения
        /// </summary>
        /// <param name="_msg">Исходная строка</param>
        /// <returns>Массив параметров</returns>
        public static string[] GetAllParams(string _msg)
        {
            string flag = _msg.Split(',')[0] + ',';
            _msg = _msg.Replace("<EOM>", "").Replace(flag, "");
            return _msg.Split(',');
        }
    }
}
