﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flag
{

    public enum Flags
    {
        Send,
        Recieve,
        Disconnect,
        GiveMeInfoPls,
        LogIn
    }

    public class Converter
    {
        public static string ConvertToRead(string _msg)
        {
           return _msg = _msg.Replace("<EOM>", "");
        }

        public static string ConvertToSend(string _msg, Flags _flag)
        {
            return _msg =_flag.ToString()+","+ _msg + "<EOM>";
        }

        public static string ConvertToSend(string _msg)
        {
            return _msg =  _msg + "<EOM>";
        }

        public static string ConvertToSend(Flags _flag)
        {
            return _flag.ToString() + "," + "<EOM>";
        }

        public static string GetFlag(string _msg)
        {
            return _msg.Split(',')[0];
        }

        public static string GetValueWithFlag(string _msg, int _id)
        {
            return _msg.Split(',')[_id + 1];
        }

        public static string GetValue(string _msg, int _id)
        {
            return _msg.Split(',')[_id];
        }
    }
}
